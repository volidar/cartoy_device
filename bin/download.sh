#! /bin/sh

rm -f ../web/*.css ../web/*.js

wget http://localhost:9000/lib/jquery/jquery.min.js -O ../jquery.min.js
wget http://localhost:9000/lib/jquery-ui/jquery-ui.min.js -O ../jquery-ui.min.js
wget http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css -O ../jquery-ui.css

wget http://localhost:9000/assets/client-launcher.js -O ../client-launcher.js
#wget http://localhost:9000/assets/client-opt.js -O ../client-opt.js
