var devServer = {};
var appVersion = "0";
var bootIdx = Sys.conf.volidar.bootIdx;

var logTraceIndent = 0;
function logTrace(method, params, code) {
  var indentStr = "";
  for(var i = 0; i < logTraceIndent; i++) indentStr += "  ";
  print(indentStr + ">", method, params);
  logTraceIndent++;
  var result = "error";
  try {
    result = code();
  } finally {
    logTraceIndent--;
    if(logTraceIndent < 0) logTraceIndent = 0;
    print(indentStr + "<", method, "=", result);
  }
  return result;
}
function logDebug(msg) {
  print(msg);
}
function logError(msg) {
  print(msg);
}

function initPin(pin, level) {
  logTrace("initPin", [pin, level], function() {
    GPIO.setMode(pin, GPIO.OUT, 0);
    setPin(pin, level);
  });
}
function setPin(pin, level) {
  logTrace("setPin", [pin, level], function() {
    GPIO.write(pin, level);
  });
}

function parseReq(query) {
/*
  logTrace("parseReq", [query], function(){ DOIT make logTrace able to work with results
*/
    var obj = {};
    var pairs = query.split('&');
    for (var i = 0; i < pairs.length; i++) {
      var x = pairs[i].split('=', 2);
      obj[x[0]] = x[1];
    }
    return obj;
/*
  });
*/
}
function sendResponse(res, code, message) {
  logTrace("parseReq", ["res", code, message], function(){
    res.writeHead(code, {'Content-Type': 'text/plain'});
    res.write(message);
    res.end();
  });
}

(function (){
  function updateBootIdx() {
    Sys.conf.pianoCat.bootIdx = bootIdx + 1;
    Sys.conf.save(false);
  }

  function pullDownPins() {
    logTrace("pullDownPins", [], function(){
      /*1 and 3 are used for UART*/
      initPin(0, false);
      /*initPin(1, false);*/
      initPin(2, false);
      /*initPin(3, false);*/
    });
  }

  function devServerFunction(req, res){
    logDebug(req);
    if(req.url == '/run') {
      var result;
      try {
        result = eval(req.body);
        logDebug(result);
      }
      catch(err) {
        logDebug(err);
        result = err;
      }
      sendResponse(res, 200, result);
    } else if(req.url == '/reboot') {
      sendResponse(res, 200, "OK");
      setTimeout(Sys.reboot, 100);
    }
  }

  var nextWifiIdx = 0;
  function setupWifi(){
    logTrace("setupWifi", [], function(){
      var ssid = Sys.conf.volidar.wifis[nextWifiIdx].ssid;
      var pass = Sys.conf.volidar.wifis[nextWifiIdx].pass;
      logDebug("-Wifi.setup(" + ssid + ", [password]): " + Wifi.setup(ssid, pass, {permanent: false}));
      setTimeout(checkWifi, 1000);
      nextWifiIdx++;
      if(nextWifiIdx >= Sys.conf.volidar.wifis.length) nextWifiIdx = 0;
    });
  }

  var MAX_ATTEMPT = 15;
  var attempt = 0;
  function checkWifi(){
    if(Wifi.status() == "got ip") {
      logDebug("+checkWifi: " + Wifi.ip());
      if(Sys.conf.volidar.devServerPort != -1) {
        logDebug("-Http.createServer");
        devServer = Http.createServer(devServerFunction).listen(Sys.conf.volidar.devServerPort);
      }
      logTrace("onWifi.js", [], function(){
        try {
          File.eval("onWifi.js");
        } catch (e) {
          logError(e);
        }
      });
    } else if(attempt < MAX_ATTEMPT) {
      logDebug(".checkWifi");
      attempt++;
      setTimeout(checkWifi, 1000);
    } else {
      logDebug("!checkWifi");
      attempt = 0;
      Wifi.disconnect();
      setTimeout(setupWifi, 2000);
    }
  }

  logTrace("init", [], function(){
    updateBootIdx();
    pullDownPins();
    logTrace("onStart.js", [], function(){
      try {
        File.eval("onStart.js");
      } catch (e) {
        logError(e);
      }
    });
    setTimeout(setupWifi, 1000);
  });
})();
