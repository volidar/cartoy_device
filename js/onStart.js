logDebug("onStart.js");

var motorL = {
  a: 5, /*D1*/
  b: 4, /*D2*/
  p: 0 /*D3*/
};
var motorR = {
  a: 14, /*D5*/
  b: 12, /*D6*/
  p: 13 /*D7*/
};
var motorD = {
  a: 16, /*D0*/
  b: 15, /*D8*/
  p: 2  /*D4*/
};
var motors = [motorL, motorR, motorD];

function process(data) {
  logTrace("process", [data], function(){
    var speed = parseInt(data.s);
    var direction = parseInt(data.d);

    resetPwm();
    setMotor(motorL, speed);
    setMotor(motorR, speed);
    setMotor(motorD, direction);
  });
}
function resetPwm() {
  logTrace("resetPwm", [], function(){
    for(var i = 0; i < motors.length; i++) {
      PWM.set(motors[i].p, 0, 0);
    }
  });
}
function setMotor(motor, value) {
  logTrace("setMotor", [motor, value], function(){
    var high;
    var low;
    var speed;

    if(value < 0) {
      high = motor.b;
      low = motor.a;
      speed = -value -1;
    } else {
      high = motor.a;
      low = motor.b;
      speed = value;
    }
    if(speed > 127) speed = 127;

    if(speed == 0) {
      GPIO.write(high, false);
      GPIO.write(low, false);
    } else {
      GPIO.write(high, true);
      GPIO.write(low, false);
    }
    if(motor.p != -1) {
      if(speed == 0) GPIO.write(motor.p, false);
      else if(speed == 127) GPIO.write(motor.p, true);
      else PWM.set(motor.p, 6350, speed * 50);
    }
  });
}

(function (){
  for(var i = 0; i < motors.length; i++) {
    GPIO.setMode(motors[i].a, GPIO.OUT, GPIO.PULLDOWN);
    GPIO.setMode(motors[i].b, GPIO.OUT, GPIO.PULLDOWN);
    GPIO.setMode(motors[i].p, GPIO.OUT, GPIO.PULLDOWN);
  }
  resetPwm();

  /* DOIT start conditionally by conf: debug code */
  var modeIdx = 0;
  var modes = ["d=0&s=0", "d=0&s=8", "d=0&s=32", "d=0&s=127", "d=-128&s=32", "d=127&s=32", "d=0&s=0", "d=0&s=-32", "d=0&s=-128", "d=0&s=-32"];

  function setMode() {
    var mode = modes[modeIdx++];
    logDebug("- Setting mode: " + mode);
    process(parseReq(mode));
    if(modeIdx >= modes.length) modeIdx = 0;
  }

/*
  setInterval(setMode, 2000);
*/
}());
