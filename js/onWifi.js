logDebug("onWifi.js");

var botServer = {};

(function (){
  function botController(req, res) {
    print(req);
    var url = req.url;
    if(url == '/bot') {
      var parsedReq;
      try {
        parsedReq = parseReq(req.body);
        process(parsedReq);
        sendResponse(res, 200, "OK");
      }
      catch(err) {
        print(err);
        sendResponse(res, 500, err);
      }
    } else if(url == "/" || url == "/client-launcher.js" || url == "/client-opt.js" || url == "/index.html" || url == "/jquery.min.js" || url == "/jquery-ui.css" || url == "/jquery-ui.min.js"){
      res.serve();
    } else {
       sendResponse(res, 404, "Not found");
    }
  }

  botServer = Http.createServer(botController).listen(80);
})();
