(function () {
  for(var pin = 0; pin < 17; pin++) {
    if(pin != 1 && pin != 3 && pin != 9 && pin != 10) {
      GPIO.setMode(pin, GPIO.OUT, GPIO.PULLDOWN);
      GPIO.write(pin, false);
    }
  }

  function set(pin) {
    GPIO.write(pin, true);
  }

  set(0);
  set(13);

  var side = false;
  if(side) set(5); else set(4);
  if(side) set(14); else set(12);
  if(side) set(16); else set(15);
})();
